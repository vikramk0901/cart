# Questions
- How many different sizes are available?
- How many different types of fruits are available?
  - What are the payment options involved?
- Can users buy without logging in? (can guest profile allow buying?)
  - Is there a limit on how much a user can buy?
- Which devices are of target?(Mobile,Desktop or web?)
  - What is the maximum time allowed for a user to complete the
  transaction?
  - What are the various ways to arrange the fruits?
  - What are the options available to make basket look good?
  - How many different type of colors available for basket?
  - Do you need sliders to set the size and number of fruits?
  - Is there a gift packing involved?
  - Will the site allows gift coupons? If so what are the various coupon
  rules?
  - How do we get the inventory data? (How many fruits are ready to be
      delivered?)
  - Do users get to track the delivery? (Involves adding new page to
      track the delivery)
  - Is there a delivery charge?
  - Api Questions
  - Do we have an api to fetch relevant meta data to construct
  the store?
- Where do we post the confirmed purchases? ()
# Design
  - UI framework should have following capabilties
  - It needs to have MVC architecture (Separtaion of
      concerns)
  - Should be reactive which reduces the boilerplate to
  implement realtime tracking of cart information
  - Should implement redux in order to make data
  behave consistently
  - Should implement Test Driven Development


# Architecture
  - The application should be hosted on a clound platform so that
    scaling for incoming traffic should be smooth
 - CI and CD should be implemented so that there are no issues with
   deployment of the latest code to the server
 - Every service notifications should be integrated into the
   communication tool like slack ()
    CI notifications
    Github notifications
    new relic notificaitons
 - Database : MongoDB
 - UI Framework : Vuejs (Easy to scale up)
