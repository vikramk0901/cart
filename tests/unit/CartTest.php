<?php


class CartTest extends PHPUnit\Framework\TestCase
{

  public function testTrueAssersToTrue()
  {
    $this->assertTrue(true);
  }

  public function testThatBasketObjHasCorrectParams()
  {
    $basket = new \App\Basket(20, 'aaple', 20, []);
    $this->assertEquals($basket->max(),20);
    $this->assertEquals($basket->type(),'aaple');
    $this->assertTrue($basket->hasCoupon());
  }

  public function testTheCostofBasket()
  {
    $basket = new \App\Basket(20, 'aaple', 10, []);
    $basket->increment();
    $this->assertEquals($basket->size(),1);
    $this->assertEquals($basket->worth(), 10);
  }

  public function testCartAddBasket()
  {
    $cart = new \App\Cart();
    $basket = new \App\Basket(2,'orange',1,[]);
    $cart->addBasket($basket);

    $this->assertEquals($cart->size(), 1);
    $tempBasket = $cart->removeBasket($basket);
    $this->assertSame($basket, $tempBasket);
  }

  public function testCartWithOneOrangeAndTenAppleDiscount()
  {
    $cart = new \App\Cart();
    $basket = new \App\Basket(2,'orange',1,[]);
    $basket->increment();
    $cart->addBasket($basket);
    $basket = new \App\Basket(2,'aaple',10,[]);
    $basket->increment();
    $cart->addBasket($basket);

    $this->assertEquals($cart->calculateDiscount(),0.5);

  }

  public function testCartWithTenOrangeAndOneAppleDiscount()
  {
    $cart = new \App\Cart();
    $basket = new \App\Basket(2,'orange',10,[]);
    $basket->increment();
    $cart->addBasket($basket);
    $basket = new \App\Basket(2,'aaple',1,[]);
    $basket->increment();
    $cart->addBasket($basket);

    $this->assertEquals($cart->calculateDiscount(),0);

  }

  public function testCartWithOnlyAppleBaskets()
  {
    $cart = new \App\Cart();
    $basket = new \App\Basket(10,'aaple',8,[]);
    $basket->add(10);
    $cart->addBasket($basket);
    $basket = new \App\Basket(5,'aaple',8,[]);
    $basket->add(5);
    $cart->addBasket($basket);

    $this->assertEquals($cart->calculateDiscount(),20);

  }

  public function testCartWithEqualWorthOfAppleAndOrange()
  {
    $cart = new \App\Cart();
    $basket = new \App\Basket(5,'aaple',16,[]);
    $basket->add(5);
    $cart->addBasket($basket);
    $basket = new \App\Basket(10,'orange',8,[]);
    $basket->add(10);
    $cart->addBasket($basket);

    $this->assertEquals($cart->calculateDiscount(),40);

  }

  public function testCartWithOrangeWorthMoreThanApple()
  {
    $cart = new \App\Cart();
    $basket = new \App\Basket(10,'orange',80,[]);
    $basket->add(10);
    $cart->addBasket($basket);
    $basket = new \App\Basket(5,'aaple',16,[]);
    $basket->add(5);
    $cart->addBasket($basket);

    $this->assertEquals($cart->calculateDiscount(),0);

  }

  public function testCartWithTwoAppleTwoOrange()
  {
    $cart = new \App\Cart();
    $basket = new \App\Basket(10,'orange',2,[]);
    $basket->add(10);
    $cart->addBasket($basket);
    $basket = new \App\Basket(5,'orange',2,[]);
    $basket->add(5);
    $cart->addBasket($basket);
    $basket = new \App\Basket(5,'aaple',16,[]);
    $basket->add(5);
    $cart->addBasket($basket);
    $basket = new \App\Basket(10,'aaple',16,[]);
    $basket->add(10);
    $cart->addBasket($basket);

    $this->assertEquals($cart->calculateDiscount(),50);
    // 160 + 80 + 10 + 20 (savings 50)

  }
}
