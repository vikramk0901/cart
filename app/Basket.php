<?php

namespace App;

class Basket
{
  private $basketSize;
  private $fruitType;
  private $decorators;
  private $pricePerFruit;
  private $count = 0;

  public function __construct($basketSize, $fruitType, $cost, $decorators){
    // @TODO VALIDATE the data in here
    $this->basketSize = $basketSize;
    $this->fruitType = $fruitType;
    $this->decorators = $decorators;
    $this->pricePerFruit = $cost;
  }

  public function hasCoupon(){
    return ($this->fruitType === 'aaple');
  }

  public function type(){
    return $this->fruitType;
  }

  // Return the number of fruits added to the basket
  public function add($fruitCount){
    if(!$this->checkCountType($fruitCount)){
      echo "Cannot add non integer number of fruit types \n";
      return 0;
    }
    if(($this->count + $fruitCount) > $this->basketSize){
      echo "There is not enough space in the basket please increase the size of your basket";
      return 0;
    }
    $this->count += $fruitCount;
    return $fruitCount;
  }

  //@TODO should be throw and error or gracefully ignore and proceed?
  private function checkCountType($fruitCount){
    return is_int($fruitCount);
  }

  public function increment(){
    $this->add(1);
  }

  public function decrement(){
    $this->remove(1);
  }

  public function max(){
    return $this->basketSize;
  }

  public function size(){
    return $this->count;
  }

  public function worth(){
    return $this->count * $this->pricePerFruit;
  }

  public function remove($fruitCount){
    if(!$this->checkCountType($fruitCount)){
      echo "Cannot remove non integer number of fruit types \n";
      return 0;
    }
    if($fruitCount > $this->count){
      echo "Do not have enough fruits to delete. Please add to delete :) \n";
      return 0;
    }

    $this->count -= $fruitCount;
    return $fruitCount;
  }

}
