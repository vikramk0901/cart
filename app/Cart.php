<?php

namespace App;

use \App\Basket;

class Cart
{
  private $startTime;
  private $baskets = [];
  private $discount = 0;

  public function __construct(){
    $this->startTime = time();
  }

  // returns number of seconds elapsed from the Cart instatiation
  public function timeElapsed(){
    return time() - $this->startTime;
  }

  public function size(){
    return count($this->baskets);
  }

  public function addBasket(Basket $basket){
    $this->baskets[] = $basket;
    return this;
  }

  public function removeBasket(Basket $basket){
    $find = array_search($basket, $this->baskets, true);
    if($find === FALSE){
      echo "Cannot find the basket in the basket list \n";
    }

    $deletedBasket = $this->baskets[$find];
    unset($this->baskets[$find]);

    return $deletedBasket;
  }

  public function applyDiscount($discount){
    $this->discount = $discount;
    return this;
  }

  private function sortBaskets($baskets = array()){
    usort($baskets, function($a, $b){
      if( $a->worth() == $b->worth()){
        //Make sure if the prices are equal the apple basket should come first
        if($a->hasCoupon()){
          return -1;
        }
        return 1;
      }

      return ($a->worth() > $b->worth()) ? -1 : 1;
    });

    return $baskets;
  }

  public function calculateDiscount(){
    $baskets = $this->sortBaskets($this->baskets);
    $currentCoupon = 0;
    $totalDiscount = 0;
    foreach($baskets as $basket){
      $totalDiscount += $basket->worth() * $currentCoupon;
      $currentCoupon = 0;
      if($basket->hasCoupon()){
        $currentCoupon = 1/2;
      }
    }

    return $totalDiscount;
  }

  public function checkout(){
  }

}
